﻿//================================================================================
//!	@file	 CommonData.cs
//!	@brief	 シーン間共通データの管理
//! @details 
//!	@author  Kai Araki									@date 2019/08/29
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   シーン間共通データを管理する
//! @details ・Singleton
//************************************************************
public class CommonData
{
	// 変数
	private static ChangeSceneCommonData instance_ = null;						//!< インスタンス


	//----------------------------------------
	//! @brief   インスタンスプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public static ChangeSceneCommonData Instance
	{
		get
		{
			if (instance_ == null)
			{
				instance_ = new ChangeSceneCommonData();
			}
			return instance_;
		}
	}

	//----------------------------------------
	//! @brief   自己インスタンス以外のデータを初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public void Init()
	{
	
	}
}