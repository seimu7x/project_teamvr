﻿//================================================================================
//!	@file	 AudioClip_SE.cs
//!	@brief	 SE用スクリプタブルオブジェクト
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//************************************************************														   
//! @brief   SE用スクリプタブルオブジェクト
//! @details ・Singleton
//************************************************************
[CreateAssetMenu(menuName = "ScriptableObject/AudioClip_SE", fileName = "AudioClip_SE")]
public class AudioClip_SE : ScriptableObject
{
	// 変数
	private static AudioClip_SE instance_ = null;     //!< インスタンス

	[SerializeField]
	AudioClip kClick = null;		//!< クリック音

	//----------------------------------------
	//! @brief   インスタンスプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public static AudioClip_SE Instance
	{
		get
		{
			if (instance_ == null)
			{
				var asset = Resources.Load("ScriptableObject/AudioClip_SE") as AudioClip_SE;

				instance_ = asset;
			}
			return instance_;
		}
	}

	//----------------------------------------
	//! @brief クリック音プロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public AudioClip Click
	{
		get
		{
			return kClick;
		}
	}
}
