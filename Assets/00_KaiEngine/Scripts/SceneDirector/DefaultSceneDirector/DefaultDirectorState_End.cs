﻿//================================================================================
//!	@file	 DefaultDirectorState_End.cs
//!	@brief	 既定シーン管理者の終了処理用ステート
//! @details
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   既定シーン管理者の終了処理を行うステート
//! @details ・マウスかDefaultPadの入力で既定シーンへ遷移する
//************************************************************
public class DefaultDirectorState_End : DefaultDirectorState
{
	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   既定シーンへ遷移する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		DefaultDirector.FadeManager.StartFadeOut("DefaultScene");
	}
}
