﻿//================================================================================
//!	@file	 DefaultDirectorState.cs
//!	@brief	 既定シーン管理者用ステート
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   既定シーン管理者用ステート
//! @details ・Abstract
//************************************************************
public abstract class DefaultDirectorState : SceneState
{
	//----------------------------------------
	//! @brief 既定シーン管理者インスタンスプロパティ
	//! @details
	//! @param void なし
	//! @retval void なし
	//----------------------------------------
	public DefaultDirector DefaultDirector { protected get; set; }

	//----------------------------------------
	//! @brief   管理者を既定シーン管理者にキャストする
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void CastDirector(SceneDirector scene_director)
	{
		DefaultDirector = (DefaultDirector)scene_director;
	}
}