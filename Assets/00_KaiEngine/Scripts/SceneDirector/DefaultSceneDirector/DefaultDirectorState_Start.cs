﻿//================================================================================
//!	@file	 DefaultDirectorState_Start.cs
//!	@brief	 既定シーン管理者の初期化用ステート
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//************************************************************														   
//! @brief   既定シーン管理者の初期化を行うステート
//! @details ・フェードイン終了後、通常ステートへ移行する
//************************************************************
public class DefaultDirectorState_Start : DefaultDirectorState
{
	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   フェードイン終了後、通常ステートへ移行する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		if (DefaultDirector.FadeManager.IsFadeIn()) return;
		DefaultDirector.State = new DefaultDirectorState_Normal();
	}
}
