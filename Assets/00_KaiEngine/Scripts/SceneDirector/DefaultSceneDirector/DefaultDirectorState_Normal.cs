﻿//================================================================================
//!	@file	 DefaultDirectorState_Normal.cs
//!	@brief	 既定シーン管理者の通常用ステート
//! @details
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   既定シーン管理者の通常処理を行うステート
//! @details ・マウスかDefaultPadの入力で終了処理ステートへ遷移する
//************************************************************
public class DefaultDirectorState_Normal : DefaultDirectorState
{
	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   マウスかDefaultPadの入力で終了処理ステートへ遷移する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		if (Input.GetMouseButtonDown(0) || DefaultDirector.GamePad.TriggerButton("START"))
		{
			DefaultDirector.State = new DefaultDirectorState_End();
			DefaultDirector.AudioSource.PlayOneShot(AudioClip_SE.Instance.Click);
		}
	}
}
