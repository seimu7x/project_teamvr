﻿//================================================================================
//!	@file	 SceneDirector.cs
//!	@brief	 シーンの管理者
//! @details 
//!	@author  Kai Araki									@date 2019/08/29
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//************************************************************														   
//! @brief   シーン全体の流れを管理する
//! @details ・Abstract
//! @details ・各シーン管理者の基底クラスになる
//************************************************************
public abstract class SceneDirector : MonoBehaviour
{
	// 変数
	SceneState state_;       //!< ステート
	
	//----------------------------------------
	//! @brief   ステートプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public SceneState State
	{
		get
		{
			return state_;
		}
		set
		{
			// ステートの終了処理
			if (state_ != null)
			{
				state_.Uninit();
			}

			// ステートの代入
			state_ = value;
			if (state_ == null) return;

			// ステートの初期化
			CastStateTemp_CastDirector(this);
			state_.Init();
		}
	}

	//----------------------------------------
	//! @brief   フェードマネージャプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public FadeManager FadeManager { get; private set; }

	//----------------------------------------
	//! @brief   ゲームパッドプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public GamePad GamePad { get; private set; }

	//----------------------------------------
	//! @brief   オウディオソースプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public AudioSource AudioSource { get; private set; }

	//----------------------------------------
	//! @brief   ステートを一時的にキャストして管理者をキャストする
	//! @details 
	//! @param   scene_director : シーン管理者
	//! @retval  void : 無し
	//----------------------------------------
	public abstract void CastStateTemp_CastDirector(SceneDirector scene_director);

	//----------------------------------------
	//! @brief   必要なオブジェクトの取得及びステート初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public virtual void Start()
	{
		// 各種取得
		FadeManager = GameObject.Find("FadeCanvas/FadeCurtain").GetComponent<FadeManager>();
		GamePad = GameObject.Find("GamePad").GetComponent<GamePad>();
		AudioSource = GetComponent<AudioSource>();

		// ステートの初期化
		State = null;
	}
	
	//----------------------------------------
	//! @brief   ステートの終了処理
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void OnDestroy()
	{
		if (state_ == null) return;
		state_.Uninit();
		state_ = null;
	}

	//----------------------------------------
	//! @brief   ステートを毎フレーム更新
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Update()
	{
		if (state_ == null) return;
		state_.Update();
	}
}