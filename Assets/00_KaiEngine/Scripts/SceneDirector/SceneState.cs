﻿//================================================================================
//!	@file	 SceneState.cs
//!	@brief	 シーン管理者用ステート
//! @details 
//!	@author  Kai Araki									@date 2019/08/29
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   シーン管理者用ステート
//! @details ・Abstract
//************************************************************
public abstract class SceneState
{
	//----------------------------------------
	//! @brief   初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public abstract void Init();

	//----------------------------------------
	//! @brief   終了処理
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public abstract void Uninit();

	//----------------------------------------
	//! @brief   毎フレームの更新処理
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public abstract void Update();

	//----------------------------------------
	//! @brief   シーン管理者をキャストする
	//! @details
	//! @param   scene_director : シーン管理者
	//! @retval  void : 無し
	//----------------------------------------
	public abstract void CastDirector(SceneDirector scene_director);
}