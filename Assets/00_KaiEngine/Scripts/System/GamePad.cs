﻿//================================================================================
//!	@file	 GamePad.cs
//!	@brief	 GamePad入力管理
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   GamePadからの入力を処理する
//! @details ・毎フレームGamePadから入力を取得
//! @details ・1フレーム前のGamePadから入力を保持し比較
//************************************************************
public class GamePad : MonoBehaviour
{
	//########################################														   
	//! @brief   保存する入力データ群
	//! @details  
	//########################################
	struct SaveInputData
	{
		public Vector2 l_stick;             //!< Lスティック
		public Vector2 r_stick;             //!< Rスティック
		public Vector2 trigger_button;      //!< R2,L2トリガー
	}

	//########################################														   
	//! @brief   アナログスティックの方向を識別する
	//! @details  
	//########################################
	public enum StickDirection
	{
		kNone = -1,
		kUp,
		kDown,
		kLeft,
		kRight,
		kMax
	}

	//########################################														   
	//! @brief   アナログ入力の各ボタンを識別する
	//! @details  
	//########################################
	public enum AnalogDataType
	{
		kNone = -1,
		kLStickX,
		kLStickY,
		kRStickX,
		kRStickY,
		kL2Button,
		kR2Button,
		kMax
	}


	// 定数
	const float kLStickTriggerThreshold	         = 0.8f;        //!< Lスティックトリーガしきい値
	const float kLStickHoldThreshold	         = 0.0001f;     //!< Lスティックホールドしきい値
	const float kRStickTriggerThreshold	         = 0.8f;        //!< Rスティックトリーガしきい値
	const float kRStickHoldThreshold	         = 0.0001f;     //!< Rスティックホールドしきい値
	const float kL2ButtonTriggerReleaseThreshold = 0.8f;        //!< L2トリガーボタントリーガリリースしきい値
	const float kL2ButtonHoldThreshold	         = 0.0001f;     //!< L2トリガーボタンホールドしきい値
	const float kR2ButtonTriggerReleaseThreshold = 0.8f;        //!< R2トリガーボタントリーガリリースしきい値
	const float kR2ButtonHoldThreshold	         = 0.0001f;     //!< R2トリガーボタンホールドしきい値


	// 変数
	SaveInputData current_save_input_data_;     //!< 現フレームの保存用データ
	SaveInputData old_save_input_data_;         //!< 前フレームの保存用データ


	//----------------------------------------
	//! @brief   更新関数を1回実行
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Start()
	{
		Update();
	}

	//----------------------------------------
	//! @brief   前フレームの値を保存し、現フレームの値を取得する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Update()
	{
		// 前フレームの値を保存
		old_save_input_data_ = current_save_input_data_;

		// Lスティック
		current_save_input_data_.l_stick.x = Input.GetAxis("LStick_X");
		current_save_input_data_.l_stick.y = Input.GetAxis("LStick_Y");

		// Rスティック
		current_save_input_data_.r_stick.x = Input.GetAxis("RStick_X");
		current_save_input_data_.r_stick.y = Input.GetAxis("RStick_Y");

		// トリガーボタン
		current_save_input_data_.trigger_button.x = Input.GetAxis("L2");
		current_save_input_data_.trigger_button.y = Input.GetAxis("R2");
	}

	//----------------------------------------
	//! @brief   指定されたボタンが現フレームだけ押されているかの判定
	//! @details
	//! @param   button_name : 判定したいボタン名
	//! @retval  bool : 現フレームだけ押されていたらtrue
	//----------------------------------------
	public bool TriggerButton(string button_name)
	{
		return Input.GetButtonDown(button_name);
	}

	//----------------------------------------
	//! @brief   指定されたボタンが押されているかの判定
	//! @details
	//! @param   button_name : 判定したいボタン名
	//! @retval  bool : 押されていたらtrue
	//----------------------------------------
	public bool HoldButton(string button_name)
	{
		return Input.GetButton(button_name);
	}

	//----------------------------------------
	//! @brief   指定されたボタンが離されているかの判定
	//! @details
	//! @param   button_name : 判定したいボタン名
	//! @retval  bool : 離されていたらtrue
	//----------------------------------------
	public bool ButtonRelease(string button_name)
	{
		return Input.GetButtonUp(button_name);
	}

	//----------------------------------------
	//! @brief   指定方向にLStickがこのフレームだけ倒されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   stick_direction : 判定したい方向
	//! @retval  bool : 現フレームだけ倒されていたらtrue
	//----------------------------------------
	public bool TriggerLStick(StickDirection stick_direction)
	{
		switch (stick_direction)
		{
			case StickDirection.kUp:
			{
				if (current_save_input_data_.l_stick.y <  kLStickTriggerThreshold) return false;
				if (old_save_input_data_.l_stick.y     >= kLStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kDown:
			{
				if (current_save_input_data_.l_stick.y >  -kLStickTriggerThreshold) return false;
				if (old_save_input_data_.l_stick.y	   <= -kLStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kLeft:
			{
				if (current_save_input_data_.l_stick.x >  -kLStickTriggerThreshold) return false;
				if (old_save_input_data_.l_stick.x	   <= -kLStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kRight:
			{
				if (current_save_input_data_.l_stick.x <  kLStickTriggerThreshold) return false;
				if (old_save_input_data_.l_stick.x	   >= kLStickTriggerThreshold) return false;
				return true;
			}
			default:
			{
				return false;
			}
		}
	}

	//----------------------------------------
	//! @brief   指定方向にLStickが倒されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   stick_direction : 判定したい方向
	//! @retval  float : しきい値を超えていたら値を返す
	//----------------------------------------
	public float HoldLStick(StickDirection stick_direction)
	{
		switch (stick_direction)
		{
			case StickDirection.kUp:
			{
				if (current_save_input_data_.l_stick.y < kLStickHoldThreshold) return 0.0f;
				return current_save_input_data_.l_stick.y;
			}
			case StickDirection.kDown:
			{
				if (current_save_input_data_.l_stick.y > -kLStickHoldThreshold) return 0.0f;
				return current_save_input_data_.l_stick.y;
			}
			case StickDirection.kLeft:
			{
				if (current_save_input_data_.l_stick.x > -kLStickHoldThreshold) return 0.0f;
				return current_save_input_data_.l_stick.x;
			}
			case StickDirection.kRight:
			{
				if (current_save_input_data_.l_stick.x < kLStickHoldThreshold) return 0.0f;
				return current_save_input_data_.l_stick.x;
			}
			default:
			{
				return 0.0f;
			}
		}
	}

	//----------------------------------------
	//! @brief   指定方向にRStickがこのフレームだけ倒されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   stick_direction : 判定したい方向
	//! @retval  bool : 現フレームだけ倒されていたらtrue
	//----------------------------------------
	public bool TriggerRStick(StickDirection stick_direction)
	{
		switch (stick_direction)
		{
			case StickDirection.kUp:
			{
				if (current_save_input_data_.r_stick.y <  kRStickTriggerThreshold) return false;
				if (old_save_input_data_.r_stick.y     >= kRStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kDown:
			{
				if (current_save_input_data_.r_stick.y >  -kRStickTriggerThreshold) return false;
				if (old_save_input_data_.r_stick.y	   <= -kRStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kLeft:
			{
				if (current_save_input_data_.r_stick.x >  -kRStickTriggerThreshold) return false;
				if (old_save_input_data_.r_stick.x	   <= -kRStickTriggerThreshold) return false;
				return true;
			}
			case StickDirection.kRight:
			{
				if (current_save_input_data_.r_stick.x <  kRStickTriggerThreshold) return false;
				if (old_save_input_data_.r_stick.x	   >= kRStickTriggerThreshold) return false;
				return true;
			}
			default:
			{
				return false;
			}
		}
	}

	//----------------------------------------
	//! @brief   指定方向にRStickが倒されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   stick_direction : 判定したい方向
	//! @retval  float : しきい値を超えていたら値を返す
	//----------------------------------------
	public float HoldRStick(StickDirection stick_direction)
	{
		switch (stick_direction)
		{
			case StickDirection.kUp:
			{
				if (current_save_input_data_.r_stick.y < kRStickHoldThreshold) return 0.0f;
				return current_save_input_data_.r_stick.y;
			}
			case StickDirection.kDown:
			{
				if (current_save_input_data_.r_stick.y > -kRStickHoldThreshold) return 0.0f;
				return current_save_input_data_.r_stick.y;
			}
			case StickDirection.kLeft:
			{
				if (current_save_input_data_.r_stick.x > -kRStickHoldThreshold) return 0.0f;
				return current_save_input_data_.r_stick.x;
			}
			case StickDirection.kRight:
			{
				if (current_save_input_data_.r_stick.x < kRStickHoldThreshold) return 0.0f;
				return current_save_input_data_.r_stick.x;
			}
			default:
			{
				return 0.0f;
			}
		}
	}

	//----------------------------------------
	//! @brief   L2Buttonが現フレームだけ押されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  bool : 現フレームだけ押されていたらtrue
	//----------------------------------------
	public bool TriggerL2Button()
	{
		if (current_save_input_data_.trigger_button.x <  kL2ButtonTriggerReleaseThreshold) return false;
		if (old_save_input_data_.trigger_button.x     >= kL2ButtonTriggerReleaseThreshold) return false;
		return true;
	}

	//----------------------------------------
	//! @brief   L2Buttonが押されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  float : しきい値を超えていたら値を返す
	//----------------------------------------
	public float HoldL2Button()
	{
		if (current_save_input_data_.trigger_button.x < kL2ButtonHoldThreshold) return 0.0f;
		return current_save_input_data_.trigger_button.x;
	}

	//----------------------------------------
	//! @brief   L2Buttonが離されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  bool : 離されていたらtrue
	//----------------------------------------
	public bool ReleaseL2Button()
	{
		if (current_save_input_data_.trigger_button.x >  kL2ButtonTriggerReleaseThreshold) return false;
		if (old_save_input_data_.trigger_button.x     <= kL2ButtonTriggerReleaseThreshold) return false;
		return true;
	}

	//----------------------------------------
	//! @brief   R2Buttonが現フレームだけ押されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  bool : 現フレームだけ押されていたらtrue
	//----------------------------------------
	public bool TriggerR2Button()
	{
		if (current_save_input_data_.trigger_button.y >  -kR2ButtonTriggerReleaseThreshold) return false;
		if (old_save_input_data_.trigger_button.y     <= -kR2ButtonTriggerReleaseThreshold) return false;
		return true;
	}

	//----------------------------------------
	//! @brief   R2Buttonが押されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  float : しきい値を超えていたら値を返す
	//----------------------------------------
	public float HoldR2Button()
	{
		if (current_save_input_data_.trigger_button.y > -kR2ButtonHoldThreshold) return 0.0f;
		return current_save_input_data_.trigger_button.y;
	}

	//----------------------------------------
	//! @brief   R2Buttonが離されているかの判定
	//! @details ・バグ防止にしきい値を使用
	//! @param   void : 無し
	//! @retval  bool : 離されていたらtrue
	//----------------------------------------
	public bool ReleaseR2Button()
	{
		if (current_save_input_data_.trigger_button.y <  -kR2ButtonTriggerReleaseThreshold) return false;
		if (old_save_input_data_.trigger_button.y     >= -kR2ButtonTriggerReleaseThreshold) return false;
		return true;
	}

	//----------------------------------------
	//! @brief   指定されたアナログデータを返す
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public float GetAnalogData(AnalogDataType analog_data_type)
	{
		switch (analog_data_type)
		{
			case AnalogDataType.kLStickX:
			{
				return current_save_input_data_.l_stick.x;
			}
			case AnalogDataType.kLStickY:
			{
				return current_save_input_data_.l_stick.y;
			}
			case AnalogDataType.kRStickX:
			{
				return current_save_input_data_.r_stick.x;
			}
			case AnalogDataType.kRStickY:
			{
				return current_save_input_data_.r_stick.y;
			}
			case AnalogDataType.kL2Button:
			{
				return current_save_input_data_.trigger_button.x;
			}
			case AnalogDataType.kR2Button:
			{
				return current_save_input_data_.trigger_button.y;
			}
			default:
			{
				return 0.0f;
			}
		}
	}
}
