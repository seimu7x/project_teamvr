﻿//================================================================================
//!	@file	 LoadDirector.cs
//!	@brief	 ロードシーンの管理
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



//************************************************************														   
//! @brief   指定シーンのロードとロード画面アニメーションを管理する
//! @details ・コールチンにより非同期で次のシーンを読み込む
//************************************************************
public class LoadDirector : MonoBehaviour
{
	// 変数
	AsyncOperation async_operation_;    //!< 非同期オペーレーション
	Slider slider_;                     //!< スライダー
	Text load_num_logo_;                //!< ロード数字ロゴ


	//----------------------------------------
	//! @brief   ロード画面UIの取得及びコールチン開始
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Start()
	{
		// スライダーの取得
		slider_ = GameObject.Find("LoadCanvas/Background/Slider").GetComponent<Slider>();

		// ロード数字ロゴ
		load_num_logo_ = GameObject.Find("LoadCanvas/LoadNumLogo").GetComponent<Text>();
	
		// コールチンを開始する
		StartCoroutine("LoadDataNextScene");
	}
	
	//----------------------------------------
	//! @brief   ロード画面UIの更新とシーンの読み込み
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	IEnumerator LoadDataNextScene()
	{
		// シーンの読み込み
		async_operation_ = SceneManager.LoadSceneAsync(ChangeSceneCommonData.Instance.NextSceneName);

		// 読み込み終了まで進捗状況を更新する
		while(!async_operation_.isDone)
		{
			// バーの調整
			float progress_var = Mathf.Clamp01(async_operation_.progress / 0.9f);
			slider_.value = progress_var;
			float temp_num = slider_.value * 100;

			// ロード数字の調整
			load_num_logo_.text = temp_num.ToString("f0") + "%";

			yield return null;
		}
	}
}
