﻿//================================================================================
//!	@file	 FadeManager.cs
//!	@brief	 Fadeの管理
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



//************************************************************														   
//! @brief   FadeOut→LoadScene→FadeInまでを管理する
//! @details ・FadeOut,Inの間にLoadSceneを経由する
//! @details ・ChangeSceneCommonDataClassに共通項目を保存
//************************************************************
public class FadeManager : MonoBehaviour
{
	//########################################														   
	//! @brief   Fadeの状態を識別する
	//! @details 
	//########################################
	public enum State
	{
		kNone = -1,
		kFadeOut,
		kFadeIn,
		kFadeOutEnd,
		kFadeInEnd,
		kMax
	}


	// 定数
	const float kDefaultFadeTime = 1.0f;        //!< 既定フェード時間


	// 変数
	[SerializeField]
	private Material fade_in_material_ = null;     //!< フェードイン用マテリアル
	[SerializeField]
	private Material fade_out_material_ = null;    //!< フェードアウト用マテリアル

	private Image transition_tex_ = null;          //!< トランジション用テクスチャ
	private float current_time_   = 0.0f;          //!< 現在の時間
	private float fade_time_      = 0.0f;          //!< フェード時間
	private State state_          = State.kNone;   //!< ステート


	//----------------------------------------
	//! @brief   テクスチャの取得およびFadeIn開始又はFade終了
	//! @details 
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Start()
	{
		// トランジション用テクスチャを取得しOFFにする
		transition_tex_ = GetComponent<Image>();
		transition_tex_.enabled = false;

		// Stateの変更
		if (ChangeSceneCommonData.Instance.SaveFadeState == State.kFadeOutEnd)
		{
			// Stateがフェードアウトの場合はフェードイン
			StartFadeIn();
		}
		else
		{
			// その他はkNone
			state_ = State.kNone;
		}
	}

	//----------------------------------------
	//! @brief   各Stateごとの処理を実行
	//! @details 
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void Update()
	{
		// Stateごとの更新
		switch (state_)
		{
			case State.kFadeOut:
			{
				AddAlpha_TokFadeOutEndStateWhenAlphaIsMax();
				break;
			}
			case State.kFadeIn:
			{
				SubAlpha_TokFadeInEndStateWhenAlphaIsMin();
				break;
			}
			case State.kFadeOutEnd:
			{
				SaveStateTokFadeOutEndState_ChangeLoadScene();
				break;
			}
			case State.kFadeInEnd:
			{
				SaveStateTokFadeInEndState_TokNoneState_TexEnableOff();
				break;
			}
		}

		// エスケープ
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			// 終了処理
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
			Application.OpenURL("http://www.yahoo.co.jp/");
#else
			Application.Quit();
#endif
		}
	}

	//----------------------------------------
	//! @brief   α値を上げ、MaxになったらkFadeOutEndStateへ変更
	//! @details ・Maxになる速度はStartFadeOut()で指定した時間に依存
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void AddAlpha_TokFadeOutEndStateWhenAlphaIsMax()
	{
		if (current_time_ < fade_time_)
		{
			// α値の更新
			fade_out_material_.SetFloat("_Alpha", current_time_ / fade_time_);

			// 現在時刻の更新
			current_time_ += Time.deltaTime;
		}
		else
		{
			// ステートの更新
			state_ = State.kFadeOutEnd;

			// バグ防止にα値を1に変更
			fade_out_material_.SetFloat("_Alpha", 1);
		}
	}

	//----------------------------------------
	//! @brief   α値を下げ、MinになったらkFadeInEndStateへ変更
	//! @details ・Minになる速度はStartFadeIn()で指定した時間に依存
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void SubAlpha_TokFadeInEndStateWhenAlphaIsMin()
	{
		if (current_time_ < fade_time_)
		{
			// α値の更新
			fade_in_material_.SetFloat("_Alpha", current_time_ / fade_time_);

			// 現在時刻の更新
			current_time_ += Time.deltaTime;
		}
		else
		{
			// ステートの更新
			state_ = State.kFadeInEnd;

			// バグ防止にα値を1に変更
			fade_in_material_.SetFloat("_Alpha", 1);
		}
	}

	//----------------------------------------
	//! @brief   保存StateをkFadeOutEndStateに変更及びロードシーンへの変更
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void SaveStateTokFadeOutEndState_ChangeLoadScene()
	{
		// 保存Stateの更新
		ChangeSceneCommonData.Instance.SaveFadeState = State.kFadeOutEnd;

		// ロードシーンヘ変更
		SceneManager.LoadScene("LoadScene");
	}

	//----------------------------------------
	//! @brief   保存StateをkFadeInEndStateに変更及びStateをkNoneに変更、トランジション用テクスチャOFF
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	void SaveStateTokFadeInEndState_TokNoneState_TexEnableOff()
	{
		// 保存Stateの更新
		ChangeSceneCommonData.Instance.SaveFadeState = State.kFadeInEnd;

		// ステートの変更
		state_ = State.kNone;

		// イメージOFF
		transition_tex_.enabled = false;
	}

	//----------------------------------------
	//! @brief   フェードアウトを開始する各種処理を行う
	//! @details ・フェードのStateがkNoneのときは何も実行しない
	//! @param   next_scene_name : 次のシーン名
	//! @param   fade_time       : フェード時間
	//! @retval  void : 無し
	//----------------------------------------
	public void StartFadeOut(string next_scene_name, float fade_time = kDefaultFadeTime)
	{
		if (state_ != State.kNone) return;

		// 現在の時間をリセット
		current_time_ = 0.0f;

		// フェード時間の設定
		fade_time_ = fade_time;

		// マテリアルの変更
		transition_tex_.material = fade_out_material_;

		// α値を0にする(バグ防止)
		fade_out_material_.SetFloat("_Alpha", 0);

		// ステートの変更
		state_ = State.kFadeOut;

		// トランジションテクスチャON
		transition_tex_.enabled = true;

		// 次のシーンを設定
		ChangeSceneCommonData.Instance.NextSceneName = next_scene_name;
	}

	//----------------------------------------
	//! @brief   フェードインを開始する各種処理を行う
	//! @details ・フェードのStateがkNoneのときは何も実行しない
	//! @param   fade_time : フェード時間
	//! @retval  void : 無し
	//----------------------------------------
	public void StartFadeIn(float fade_time = kDefaultFadeTime)
	{
		// 現在の時間をリセット
		current_time_ = 0.0f;

		// フェード時間の設定
		fade_time_ = fade_time;

		// マテリアルの変更
		transition_tex_.material = fade_in_material_;

		// α値を0にする(バグ防止)
		fade_in_material_.SetFloat("_Alpha", 0);

		// ステートの変更
		state_ = State.kFadeIn;

		// トランジションテクスチャON
		transition_tex_.enabled = true;
	}

	//----------------------------------------
	//! @brief   フェードアウトが終了しているかの確認
	//! @details
	//! @param   void : 無し
	//! @retval  bool : フェードアウト終了でtrue
	//----------------------------------------
	public bool IsFadeOutEnd()
	{
		return state_ == State.kFadeOutEnd;
	}

	//----------------------------------------
	//! @brief 　フェードイン中かの確認
	//! @details
	//! @param   void : 無し
	//! @retval  bool : フェードイン中でtrue
	//----------------------------------------
	public bool IsFadeIn()
	{
		return (state_ == State.kFadeIn) || (state_ == State.kFadeInEnd);
	}
}