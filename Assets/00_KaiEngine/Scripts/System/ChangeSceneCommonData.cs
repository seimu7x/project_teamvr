﻿//================================================================================
//!	@file	 ChangeSceneCommonData.cs
//!	@brief	 シーン変更のシーン間共通データの管理
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   シーン変更のシーン間共通データを管理する
//! @details ・Singleton
//************************************************************
public class ChangeSceneCommonData
{
	// 定数
	private const string kStartSceneName = "GameScene";		//!< 最初のシーン名

	// 変数
	private static ChangeSceneCommonData instance_ = null;						//!< インスタンス
	private FadeManager.State save_fade_state_     = FadeManager.State.kNone;	//!< 保存用フェードステート
	private string next_scene_name_			       = kStartSceneName;			//!< 次のシーン名


	//----------------------------------------
	//! @brief   インスタンスプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public static ChangeSceneCommonData Instance
	{
		get
		{
			if (instance_ == null)
			{
				instance_ = new ChangeSceneCommonData();
			}
			return instance_;
		}
	}

	//----------------------------------------
	//! @brief   保存用フェードステートプロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public FadeManager.State SaveFadeState
	{
		get
		{
			return save_fade_state_;
		}
		set
		{
			save_fade_state_ = value;
		}
	}

	//----------------------------------------
	//! @brief   次のシーン名プロパティ
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public string NextSceneName
	{
		get
		{
			return next_scene_name_;
		}
		set
		{
			next_scene_name_ = value;
		}
	}

	//----------------------------------------
	//! @brief   自己インスタンス以外のデータを初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public void Init()
	{
		save_fade_state_ = FadeManager.State.kNone;
		next_scene_name_ = kStartSceneName;		
	}
}