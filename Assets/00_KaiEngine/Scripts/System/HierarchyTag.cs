﻿//================================================================================
//!	@file	 HierarchyTag.cs
//!	@brief	 階層構造タグの管理
//! @details 
//!	@author  Kai Araki									@date 2019/03/02
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   タグを階層構造で扱えるようにする
//! @details ・Static
//************************************************************
public static class HierarchyTag
{
	//----------------------------------------
	//! @brief   子タグの親タグを返す、無い場合は子タグを返す
	//! @details
	//! @param   child_tag : タグ
	//! @retval  string : 親タグ又は子タグ
	//----------------------------------------
	public static string GetParentTag(string child_tag)
	{
		// ｢/｣の位置を保存
		int index = child_tag.IndexOf("/");

		// 親子関係になっている場合
		if (index > 0) return child_tag.Substring(0, index);

		// 単体の場合
		return child_tag;
	}

	//----------------------------------------
	//! @brief   ゲームオブジェクトの親タグを返す、無い場合は自分のタグを返す
	//! @details
	//! @param   game_object : ゲームオブジェクト
	//! @retval  string : 親タグ又は自分のタグ
	//----------------------------------------
	public static string GetParentTag(GameObject game_object)
	{
		return GetParentTag(game_object.tag);
	}

	//----------------------------------------
	//! @brief   親タグの子タグを返す、無い場合は親タグを返す
	//! @details
	//! @param   parent_tag : 親タグ
	//! @retval  string : 子タグ又は親タグ
	//----------------------------------------
	public static string GetChildTag(string parent_tag)
	{
		// ｢/｣の位置を保存
		int index = parent_tag.IndexOf("/");

		// 親子関係になっている場合
		if (index > 0) return parent_tag.Substring(++index);

		// 単体の場合
		return parent_tag;
	}

	//----------------------------------------
	//! @brief   ゲームオブジェクトの子タグを返す、無い場合は自分のタグを返す
	//! @details
	//! @param   game_object : ゲームオブジェクト
	//! @retval  string : 子タグ又は自分のタグ
	//----------------------------------------
	public static string GetChildTag(GameObject game_object)
	{
		return GetChildTag(game_object.tag);
	}

	//----------------------------------------
	//! @brief   親タグの付いたゲームオブジェク群を出力する
	//! @details 
	//! @param   parent_tag : 親タグ
	//! @retval  GameObject[] : 親タグの付いたゲームオブジェク群
	//----------------------------------------
	public static GameObject[] GetObjectsWithParentTag(string parent_tag)
	{
		List<GameObject> game_objects = new List<GameObject>();

		// シーン内の全オブジェクトから検索
		foreach (GameObject temp_object in GameObject.FindObjectsOfType<GameObject>())
		{
			// 検索対象のタグを持っている場合追加
			if (GetParentTag(temp_object.tag) == parent_tag)
			{
				game_objects.Add(temp_object);
			}
		}

		return game_objects.ToArray();
	}

	//----------------------------------------
	//! @brief   子タグの付いたゲームオブジェク群を出力する
	//! @details 
	//! @param   parent_tag : 子タグ
	//! @retval  GameObject[] : 子タグの付いたゲームオブジェク群
	//----------------------------------------
	public static GameObject[] GetObjectsWithChildTag(string child_tag)
	{
		List<GameObject> game_objects = new List<GameObject>();

		// シーン内の全オブジェクトから検索
		foreach (GameObject temp_object in GameObject.FindObjectsOfType<GameObject>())
		{
			// 検索対象のタグを持っている場合追加
			if (GetChildTag(temp_object.tag) == child_tag)
			{
				game_objects.Add(temp_object);
			}
		}

		return game_objects.ToArray();
	}
}
