﻿//================================================================================
//!	@file	 GameDirectorState_End.cs
//!	@brief	 ゲームシーン管理者の終了処理用ステート
//! @details
//!	@author  Kai Araki									@date 2019/10/17
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   ゲームシーン管理者の終了処理を行うステート
//! @details ・ゲームシーンへ遷移する
//************************************************************
public class GameDirectorState_End : GameDirectorState
{
	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   ゲームシーンへ遷移する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		GameDirector.FadeManager.StartFadeOut("GameScene");
	}
}
