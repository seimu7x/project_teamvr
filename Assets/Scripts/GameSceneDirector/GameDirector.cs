﻿//================================================================================
//!	@file	 GameDirector.cs
//!	@brief	 ゲームシーンの管理
//! @details 
//!	@author  Kai Araki									@date 2019/10/17
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//************************************************************														   
//! @brief   既定シーン全体の流れを管理する
//! @details 
//************************************************************
public class GameDirector : SceneDirector
{
	//----------------------------------------
	//! @brief   必要なオブジェクトの取得及びステート初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Start()
	{
		base.Start();

		// ステートの初期化
		State = new GameDirectorState_Start();
	}

	//----------------------------------------
	//! @brief   ステートを一時的にキャストして管理者をゲームシーン管理者にキャストする
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void CastStateTemp_CastDirector(SceneDirector scene_director)
	{
		GameDirectorState Game_director_state = (GameDirectorState)State;
		Game_director_state.CastDirector(scene_director);
	}
}