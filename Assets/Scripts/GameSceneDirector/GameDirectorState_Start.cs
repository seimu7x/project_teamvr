﻿//================================================================================
//!	@file	 GameDirectorState_Start.cs
//!	@brief	 ゲームシーン管理者の初期化用ステート
//! @details 
//!	@author  Kai Araki									@date 2019/10/17
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



//************************************************************														   
//! @brief   ゲームシーン管理者の初期化を行うステート
//! @details ・フェードイン終了後、通常ステートへ移行する
//************************************************************
public class GameDirectorState_Start : GameDirectorState
{
	//----------------------------------------
	//! @brief   初期化
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   フェードイン終了後、通常ステートへ移行する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		if (GameDirector.FadeManager.IsFadeIn()) return;
		GameDirector.State = new GameDirectorState_Normal();
	}
}
