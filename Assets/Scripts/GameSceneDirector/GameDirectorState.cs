﻿//================================================================================
//!	@file	 GameDirectorState.cs
//!	@brief	 ゲームシーン管理者用ステート
//! @details 
//!	@author  Kai Araki									@date 2019/10/17
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   ゲームシーン管理者用ステート
//! @details ・Abstract
//************************************************************
public abstract class GameDirectorState : SceneState
{
	//----------------------------------------
	//! @brief ゲームシーン管理者インスタンスプロパティ
	//! @details
	//! @param void なし
	//! @retval void なし
	//----------------------------------------
	public GameDirector GameDirector { protected get; set; }

	//----------------------------------------
	//! @brief   管理者をゲームシーン管理者にキャストする
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void CastDirector(SceneDirector scene_director)
	{
		GameDirector = (GameDirector)scene_director;
	}
}