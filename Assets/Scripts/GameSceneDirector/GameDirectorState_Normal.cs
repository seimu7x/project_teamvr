﻿//================================================================================
//!	@file	 GameDirectorState_Normal.cs
//!	@brief	 ゲームシーン管理者の通常用ステート
//! @details
//!	@author  Kai Araki									@date 2019/10/17
//================================================================================



//****************************************
// using文
//****************************************
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//************************************************************														   
//! @brief   ゲームシーン管理者の通常処理を行うステート
//! @details ・マウスかGamePadの入力で終了処理ステートへ遷移する
//************************************************************
public class GameDirectorState_Normal : GameDirectorState
{
	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Init()
	{
	}

	//----------------------------------------
	//! @brief   何もしない
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Uninit()
	{
	}

	//----------------------------------------
	//! @brief   マウスかGamePadの入力で終了処理ステートへ遷移する
	//! @details
	//! @param   void : 無し
	//! @retval  void : 無し
	//----------------------------------------
	public override void Update()
	{
		if (Input.GetMouseButtonDown(0) || GameDirector.GamePad.TriggerButton("START"))
		{
			GameDirector.State = new GameDirectorState_End();
			GameDirector.AudioSource.PlayOneShot(AudioClip_SE.Instance.Click);
		}
	}
}
